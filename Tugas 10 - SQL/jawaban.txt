1. Membuat Database
 create database myshop;
2. Membuat Tabel users
CREATE TABLE users(
    id int (3) AUTO_INCREMENT PRIMARY KEY,
    name varchar (255),
    email varchar(255),
    password varchar(255));
CREATE table categories (
	id int AUTO_INCREMENT PRIMARY KEY,
	name varchar (255));
    
CREATE table items(
    id	int AUTO_INCREMENT PRIMARY KEY,
	name	varchar(255),
	description varchar(255),
	price	int,
	stock	int,
	category_id	int,
    foreign key(category_id) REFERENCES categories(id));

3. Memasukan data ke database 

INSERT INTO users(id, name,email, password) values ("John dea", "john@doe.com", "john123"), ("Jenita Doe", "Jenita@doe.com", "jenita123"); 

INSERT INTO categories(name) values ("gadget"),("cloth"), ("men"), ("women"), ("branded");

INSERT INTO items(name, description,price, stock,category_id) 
values ("Sumsang ", "hape keren dari merek sumsang", "4000000", "1", "1"), 
("Uniklooh", "baju keren dari brand ternama", "500000" , "2", "2"),
("IMHO Watch","jam tangan anak yang jujur banget", "2000000" , "10" , "1"); 

4. Menampilkan isi table
a.) SELECT id, name,email FROM users
b.) SELECT id,name, description,price,stock,category_id FROM items WHERE price >1000000
c.) SELECT items.name, items.description, items.price, items.stock , items.category_id, categories.name AS kategori from items INNER JOIN categories ON items.category_id = categories.id

5. UPDATE Table
UPDATE items SET price = '2500000' 
WHERE name = 'sumsang';
